﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Agent : Objet{

    private Vector2 position;
    private static int compteur = 0;
    private int ID;
    private float health;

    public Agent(Vector2 position, float health):base()
    {
        ID = ++compteur;
        this.position = position;
        this.health = health;
    }

    //GETTERS
    public Vector2 getPosision()
    {
        return position;
    }

    public int getID()
    {
        return ID;
    }

    public float getHealth()
    {
        return health;
    }

    //SETTERS
    public void setPosision(Vector2 pos)
    {
        position = pos;
    }


    //METHODS
    public void decreaseHealth(float coup)
    {
        health -= coup;
    }

}
