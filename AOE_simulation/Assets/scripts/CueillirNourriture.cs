﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CueillirNourriture : Activite {

    Buisson buisson;
    public static float SPEED = 5f;

    public CueillirNourriture(Humain humain, Buisson b):base("Cueillir nourriture", 5, humain)
    {
        buisson = b;
    }

    override
    public Objet execute()
    {
        Objet retour;
        if (buisson == null) {
            retour = new Buisson(new Vector2(), 0, null);
        } else if (buisson.getGameObject() == null) { 
            retour = buisson;
        }
        else
        {
            if (Vector3.Distance(base.getHumain().getGameObject().transform.position, buisson.getGameObject().transform.position) > 1)
            {
                base.getHumain().getGameObject().transform.position = Vector3.MoveTowards(base.getHumain().getGameObject().transform.position, buisson.getGameObject().transform.position, SPEED * Time.deltaTime);
                base.getHumain().getGameObject().transform.position = new Vector3(base.getHumain().getGameObject().transform.position.x, base.getHumain().getGameObject().transform.position.y, (float)-0.003);
                retour = null;
            }
            else
            {
                retour = buisson.cueille();
            }
        }


        return retour;
    }

    public bool isNear()
    {
        bool retour;
        if (Vector3.Distance(base.getHumain().getGameObject().transform.position, buisson.getGameObject().transform.position) > 1)
        {
            retour = false;
        }
        else
        {
            retour = true;
        }

        return retour;
    }

}
