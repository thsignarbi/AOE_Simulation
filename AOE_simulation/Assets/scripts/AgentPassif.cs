﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AgentPassif : Objet{

    protected int points;
    private Vector2 position; //Map positions
    private GameObject gameObject;

    //CONSTRUCTORS
    public AgentPassif(int points):base()
    {
        this.points = points;
    }

    public AgentPassif(Vector2 position, int points, GameObject go)
    {
        this.position = position;
        this.points = points;
        this.gameObject = go;
    }

    //GETTERS
    public GameObject getGameObject()
    {
        return gameObject;
    }

    public Vector2 getPosition()
    {
        return position;
    }
}
