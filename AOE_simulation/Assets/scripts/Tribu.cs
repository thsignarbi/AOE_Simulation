﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tribu {

    private int ID;
    private float food;
    private int wood;
    public List<Humain> Effectif { get; set; }

    //CONSTRUCTEUR
    public Tribu(int ID, int food, int wood)
    {
        this.ID = ID;
        this.food = food;
        this.wood = wood;
        Effectif = new List<Humain>();
    }

    //GETTERS
    public float getFood()
    {
        return food;
    }

    public int getWood()
    {
        return wood;
    }

    //METHODS
    public void addFood(float addition)
    {
        food += addition;
    }

    public void addWood(int addition)
    {
        wood += addition;
    }

    public void useFood(float use)
    {
        food -= use;
    }

    public void useWood(int use)
    {
        wood -= use;
    }

    public void destroy()
    {
        foreach(Humain h in Effectif)
        {
            if(h != null)
            {
                Object.Destroy(h.getGameObject());
            }
        }
    }
}
