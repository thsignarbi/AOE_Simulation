﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CouperBois : Activite {

    Arbre arbre;
    public static float SPEED = 5f;

    public CouperBois(Humain humain, Arbre ar) : base("Couper bois", 5, humain)
    {
        arbre = ar;
    }

    override
    public Objet execute()
    {
        Objet retour;
        if (arbre == null)
        {
            retour = new Arbre(new Vector2(), 0, null);
        }
        else if (arbre.getGameObject() == null)
        {
            retour = arbre;
        }
        else
        {
            if (Vector3.Distance(base.getHumain().getGameObject().transform.position, arbre.getGameObject().transform.position) > 1)
            {
                base.getHumain().getGameObject().transform.position = Vector3.MoveTowards(base.getHumain().getGameObject().transform.position, arbre.getGameObject().transform.position, SPEED * Time.deltaTime);
                base.getHumain().getGameObject().transform.position = new Vector3(base.getHumain().getGameObject().transform.position.x, base.getHumain().getGameObject().transform.position.y, (float)-0.003);
                retour = null;
            }
            else
            {
                retour = arbre.hache();
            }
        }

        return retour;
    }

    public bool isNear()
    {
        bool retour;
        if (Vector3.Distance(base.getHumain().getGameObject().transform.position, arbre.getGameObject().transform.position) > 1)
        {
            retour = false;
        }
        else
        {
            retour = true;
        }

        return retour;
    }

}
