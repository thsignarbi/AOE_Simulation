﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Text.RegularExpressions;
using UnityEngine.UI;


public class gameManager : MonoBehaviour {

	[Header("Parameters")]
	public int ageMinimumToDie;
	public int nbMinSpawnBlueBeginning;
	public int nbMaxSpawnBlueBeginning;
	public int nbMinSpawnRedBeginning;
	public int nbMaxSpawnRedBeginning;
	public int foodBlueAtBeginning;
	public int woodBlueAtBeginning;
	public int foodRedAtBeginning;
	public int woodRedAtBeginning;
	public float healthBlue;
	public float forceBlue;
	public float healthRed;
	public float forceRed;
	public int woodToBornBlue;
	public int foodToBornBlue;
	public int woodToBornRed;
	public int foodToBornRed;
	public float useFoodBlue;
	public float useFoodRed;


	[Header("Game objects")]
    public GameObject humain_bleu;
    public GameObject humain_rouge;
    public GameObject arbre1;
    public GameObject arbre2;
    public GameObject food;
	public Text foodBlueText;
	public Text woodBlueText;
	public Text foodRedText;
	public Text woodRedText;
	public Text numberBlueText;
	public Text numberRedText;
	public Text ageBlueText;
	public Text ageRedText;
	public Text year;


    private int foodBlue;
    private int woodBlue;
    private int foodRed;
    private int woodRed;
    private int numberBlue;
    private int numberRed;
    private int ageBlue;
    private int ageRed;

    private int[,] map;
    private int tileAmount;
    private int xAmount;
    private int yAmount;
    private int extraTreeX;
    private int extraTreeY;
    private float minX;
    private float maxY;
    private Vector2 blueBase;
    private Vector2 redBase;

    public static float TEMPS = 0.10f;
    private float temps = 0;

    public List<Arbre> arbres;
    public List<Buisson> buissons;
    public List<Humain> humains;

    private Tribu tribuBleue;
    private Tribu tribuRouge;

    void Start() {

		tribuBleue = new Tribu(1, foodBlueAtBeginning, woodBlueAtBeginning);
        tribuRouge = new Tribu(2, foodRedAtBeginning, woodRedAtBeginning);

        map = GameObject.Find("LevelGenerator").GetComponent<LevelGenerator>().getMap();
        xAmount = (int)GameObject.Find("LevelGenerator").GetComponent<LevelGenerator>().getXAmount();
        yAmount = (int)GameObject.Find("LevelGenerator").GetComponent<LevelGenerator>().getYAmount();
        extraTreeX = (int)GameObject.Find("LevelGenerator").GetComponent<LevelGenerator>().getExtraTreeX();
        extraTreeY = (int)GameObject.Find("LevelGenerator").GetComponent<LevelGenerator>().getExtraTreeY();
        minX = (int)GameObject.Find("LevelGenerator").GetComponent<LevelGenerator>().getMinX();
        maxY = (int)GameObject.Find("LevelGenerator").GetComponent<LevelGenerator>().getMaxY();
        tileAmount = (int)GameObject.Find("LevelGenerator").GetComponent<LevelGenerator>().getTileAmount();

        arbres = GameObject.Find("LevelGenerator").GetComponent<LevelGenerator>().getListArbres();
        buissons = GameObject.Find("LevelGenerator").GetComponent<LevelGenerator>().getListBuissons();
        humains = new List<Humain>();

        Debug.Log("xAmount : " + xAmount + " yAmount : " + yAmount);

        SearchBases();
        SpawnBlue();
        SpawnRed();

        StartCoroutine(simulation(0.1f, 5f));
    }

    private void SearchBases()
    {
        Debug.Log("Searching for bases");
        for(int i = 0; i < xAmount; ++i)
        {
            for (int j = 0; j < yAmount; ++j)
            {
                switch (map[i, j])
                {
                    case 3:  //Blue base
                        blueBase = new Vector2(i, j);
                        Debug.Log("Blue base : " + blueBase.ToString());
                        break;
                    case 4: //Red base
                        redBase = new Vector2(i, j);
                        Debug.Log("Red base : " + redBase.ToString());
                        break;
                }
                
            }
        }
    }

    private void SpawnBlue()
    {
        int nb = Random.Range(nbMinSpawnBlueBeginning, nbMaxSpawnBlueBeginning);

        for(int i = 0; i < nb; ++i)
        {
            int sexe = Random.Range(1, 3);
            Humain created = new Humain(1, sexe, Random.Range(0f, 80f), Instantiate(humain_bleu, new Vector3(arrayToWorldX((int)blueBase.x), arrayToWorldY((int)blueBase.y), (float)-0.003), transform.rotation) as GameObject, new Vector2(arrayToWorldX((int)blueBase.x), arrayToWorldY((int)blueBase.y)), healthBlue, forceBlue);
            humains.Add(created);
            tribuBleue.Effectif.Add(created);
        }
    }

    private void SpawnRed()
    {
		int nb = Random.Range(nbMinSpawnRedBeginning, nbMaxSpawnRedBeginning);

        for (int i = 0; i < nb; ++i)
        {
            int sexe = Random.Range(1, 3);
            Humain created = new Humain(2, sexe, Random.Range(0f, 80f), Instantiate(humain_rouge, new Vector3(arrayToWorldX((int)redBase.x), arrayToWorldY((int)redBase.y), (float)-0.003), transform.rotation) as GameObject, new Vector2(arrayToWorldX((int)redBase.x), arrayToWorldY((int)redBase.y)), healthRed, forceRed);
            humains.Add(created);
            tribuRouge.Effectif.Add(created);
        }
    }

    int arrayToWorldX(int x)
    {
        return (int)(x % xAmount) - (extraTreeX / 2) + (int)minX;
    }

    int arrayToWorldY(int y)
    {
        return (int)(maxY + (extraTreeY / 2)) - (int)(y % yAmount);
    }

    IEnumerator simulation(float waitingTime, float speed)
    {
        while (true)
        {
            year.text = "Temps écoulé : " + Mathf.Floor(temps) + " ans";
            temps += TEMPS;
            int countBlue = 0;
            int countRed = 0;
            float totalAgeBlue = 0;
            float totalAgeRed = 0;
            foreach (Humain h in humains)
            {
                if(h != null)
                {
                    if (h.getActivite() == null) //Attribution d'activite
                    {
                        Activite a;
                        Humain enemy = h.isNextToEnemy(humains);
                        if (enemy != null)
                        {
                            a = new SeBattre(h, enemy);
                        }
                        else
                        {
                            if (Random.Range(0f, 4f) < 1)
                            {
                                Buisson b = trouveBuisson(h);
                                a = new CueillirNourriture(h, b);
                            }
                            else
                            {
                                Arbre arbre = trouveArbre(h);
                                a = new CouperBois(h, arbre);
                            }
                        }

                        h.setActivite(a);
                    }
                    else
                    {
                        if (Regex.Match(h.getActivite().getNom(), "Cueillir nourriture").Success || Regex.Match(h.getActivite().getNom(), "Couper bois").Success)
                        {
                            AgentPassif res = (AgentPassif)h.getActivite().execute();
                            if (res != null)
                            {
                                if (Regex.Match(h.getActivite().getNom(), "Cueillir nourriture").Success)
                                {
                                    buissons.Remove((Buisson)res);                                  
                                }
                                else if (Regex.Match(h.getActivite().getNom(), "Couper bois").Success)
                                {
                                    arbres.Remove((Arbre)res);
                                }
                                Vector2 pos = res.getPosition();
                                map[(int)pos.x, (int)pos.y] = 0;
                                h.setActivityOver();
                            }
                            else
                            {
                                if (Regex.Match(h.getActivite().getNom(), "Cueillir nourriture").Success && ((CueillirNourriture)h.getActivite()).isNear())
                                {
                                    switch (h.getTribu())
                                    {
                                        case 1:
                                            tribuBleue.addFood(20);
                                            break;
                                        case 2:
                                            tribuRouge.addFood(20);
                                            break;
                                    }
                                }
                                else if (Regex.Match(h.getActivite().getNom(), "Couper bois").Success && ((CouperBois)h.getActivite()).isNear())
                                {
                                    switch (h.getTribu())
                                    {
                                        case 1:
                                            tribuBleue.addWood(2);
                                            break;
                                        case 2:
                                            tribuRouge.addWood(2);
                                            break;
                                    }
                                }
                            }
                        }
                        else if (Regex.Match(h.getActivite().getNom(), "Se battre").Success)
                        {
                            Humain res = (Humain)h.getActivite().execute();
                            if (res != null)
                            {
                                int index = humains.FindIndex(i => i!= null && i.getID() == ((Humain)res).getID());
                                if(index != -1)
                                {
                                    humains[index] = null;
                                }                                
                                h.setActivityOver();
                            }
                        }

                    }                
                    h.vieillit();
                    bool death = false;
					if (h.getAge() > ageMinimumToDie && Random.Range(0f, 15f) < 1)
                    {
                        death = true;                      
                    }

                    switch (h.getTribu())
                    {
                        case 1: //blue
                            countBlue++;
                            totalAgeBlue += h.getAge();
							if (tribuBleue.getFood() >= useFoodBlue)
                            {
                                tribuBleue.useFood(useFoodBlue);
                            }
                            else
                            {
                                death = true;
                            }
                            break;
                        case 2: //red
                            countRed++;
                            totalAgeRed += h.getAge();
							if (tribuRouge.getFood() >= useFoodBlue)
                            {
                                tribuRouge.useFood(useFoodRed);
                            }
                            else
                            {
                                death = true;
                            }                           
                            break;
                    }
                    if (death)
                    {
                        int index = humains.FindIndex(i => i != null && i.getID() == h.getID());
                        if (index != -1)
                        {
                            Object.Destroy(h.getGameObject());
                            humains[index] = null;
                        }
                    }
                }               
            }

            numberBlueText.text = "Nombre d'unités : " + countBlue;
            numberRedText.text = "Nombre d'unités : " + countRed;
            if(countBlue == 0)
            {
                ageBlueText.text = "Moyenne d'âge : -";
            }
            else
            {
                ageBlueText.text = "Moyenne d'âge : " + Mathf.Floor(totalAgeBlue / countBlue) + " ans";
            }
            if(countRed == 0)
            {
                ageRedText.text = "Moyenne d'âge : -";
            }
            else
            {
                ageRedText.text = "Moyenne d'âge : " + Mathf.Floor(totalAgeRed / countRed) + " ans";
            }


            if (Random.Range(0f, 2.5f) < 1) //Spawn tree
            {
                Vector2 pos = getAvailablePosition();
                if (pos.x != -1 && pos.y != -1)
                {
                    int rand = Random.Range(0, 2);
                    switch (rand)
                    {
                        case 0:
                            arbres.Add(new Arbre(new Vector2(pos.x, pos.y), 30, Instantiate(arbre1, new Vector3(arrayToWorldX((int)pos.x), arrayToWorldY((int)pos.y), (float)-0.001), transform.rotation) as GameObject));
                            break;
                        case 1:
                            arbres.Add(new Arbre(new Vector2(pos.x, pos.y), 30, Instantiate(arbre2, new Vector3(arrayToWorldX((int)pos.x), arrayToWorldY((int)pos.y), (float)-0.001), transform.rotation) as GameObject));
                            break;
                    }
                    map[(int)pos.x, (int)pos.y] = 1;
                }                
            }

            if (Random.Range(0f, 5.5f) < 1) //Spawn food
            {
                Vector2 pos = getAvailablePosition();
                if (pos.x != -1 && pos.y != -1)
                {
                    buissons.Add(new Buisson(new Vector2(pos.x, pos.y), 30, Instantiate(food, new Vector3(arrayToWorldX((int)pos.x), arrayToWorldY((int)pos.y), (float)-0.001), transform.rotation) as GameObject));
                    map[(int)pos.x, (int)pos.y] = 2;
                }
            }

            if(Random.Range(0f, 20f) < 1 && tribuBleue.getWood()>woodToBornBlue && tribuBleue.getFood()>foodToBornBlue) //spawn blue Human
            {
                SpawnHuman(1);
                tribuBleue.useWood(300);
                tribuBleue.useFood(500);
            }

            if (Random.Range(0f, 20f) < 1 && tribuRouge.getWood() > woodToBornRed && tribuRouge.getFood() > foodToBornRed) //spawn red Human
            {
                SpawnHuman(2);
                tribuRouge.useWood(300);
                tribuRouge.useFood(500);
            }

            foodBlueText.text = "Nourriture : " + Mathf.Floor(tribuBleue.getFood());
            foodRedText.text = "Nourriture : " + Mathf.Floor(tribuRouge.getFood());
            woodBlueText.text = "Bois : " + tribuBleue.getWood();
            woodRedText.text = "Bois : " + tribuRouge.getWood();

            yield return new WaitForSeconds(waitingTime);
        }

    }


    private void SpawnHuman(int tribu)
    {
        int indexF = humains.FindIndex(i => i != null && i.getSexe() == 2 && i.getAge() > 18 && i.getTribu() == tribu);
        int indexM = humains.FindIndex(i => i != null && i.getSexe() == 1 && i.getAge() > 18 && i.getTribu() == tribu);
        int sexe = Random.Range(1, 3);
        if (indexF != -1 && indexM != -1)
        {
            Humain created;
            switch (tribu)
            {
                case 1:
				created = new Humain(tribu, sexe, 0, Instantiate(humain_bleu, new Vector3(arrayToWorldX((int)blueBase.x), arrayToWorldY((int)blueBase.y), (float)-0.003), transform.rotation) as GameObject, new Vector2(arrayToWorldX((int)blueBase.x), arrayToWorldY((int)blueBase.y)), healthBlue, forceBlue);
                    humains.Add(created);
                    tribuBleue.Effectif.Add(created);
                    break;
                case 2:
				created = new Humain(tribu, sexe, 0, Instantiate(humain_rouge, new Vector3(arrayToWorldX((int)redBase.x), arrayToWorldY((int)redBase.y), (float)-0.003), transform.rotation) as GameObject, new Vector2(arrayToWorldX((int)redBase.x), arrayToWorldY((int)redBase.y)), healthRed, forceRed);
                    humains.Add(created);
                    tribuRouge.Effectif.Add(created);
                    break;
            }
        }
    }


    private Vector2 getAvailablePosition()
    {
        Vector2 retour = new Vector2(-1, -1);
        int i = 0;
        int j = 0;
        bool found = false;
        while(!found && i < xAmount)
        {
            j = 0;
            while (!found && j < yAmount)
            {
                if(map[i, j] == 0 && Random.Range(0f, xAmount*yAmount) < 1)
                {
                    retour = new Vector2(i, j);
                    found = true;
                }
                ++j;
            }
            ++i;
        }

        return retour;
    }


    Buisson trouveBuisson(Humain h)
    {
        shuffleBuisson(buissons);
        foreach(Buisson b in buissons)
        {
            if(Vector3.Distance(h.getGameObject().transform.position, b.getGameObject().transform.position) < 10) {
                return b;
            }
        }

        return null;
    }


    Arbre trouveArbre(Humain h)
    {
        shuffleArbre(arbres);
        foreach (Arbre arbre in arbres)
        {
            if (Vector3.Distance(h.getGameObject().transform.position, arbre.getGameObject().transform.position) < 10)
            {
                return arbre;
            }
        }

        return null;
    }

    public void shuffleArbre(List<Arbre> ts)
    {
        var count = ts.Count;
        var last = count - 1;
        for (var i = 0; i < last; ++i)
        {
            var r = UnityEngine.Random.Range(i, count);
            var tmp = ts[i];
            ts[i] = ts[r];
            ts[r] = tmp;
        }
    }

    public void shuffleBuisson(List<Buisson> ts)
    {
        var count = ts.Count;
        var last = count - 1;
        for (var i = 0; i < last; ++i)
        {
            var r = UnityEngine.Random.Range(i, count);
            var tmp = ts[i];
            ts[i] = ts[r];
            ts[r] = tmp;
        }
    }
}
