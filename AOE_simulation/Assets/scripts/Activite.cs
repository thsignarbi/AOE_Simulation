﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Activite {

    private string nom;
    private float duree;
    private Humain humain;


    public Activite(string nom, float duree, Humain humain)
    {
        this.nom = nom;
        this.duree = duree;
        this.humain = humain;
    }

    public abstract Objet execute();

    public Humain getHumain()
    {
        return humain;
    }

    public string getNom()
    {
        return nom;
    }

}
