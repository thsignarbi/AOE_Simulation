﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Arbre : AgentPassif {

    public GameObject terre;

    public Arbre(int points):base(points)
    {
        
    }

    public Arbre(Vector2 position, int points, GameObject go):base(position, points, go)
    {

    }

    public AgentPassif hache()
    {
        AgentPassif retour;

        if (points > 0)
        {
            points--;
        }

        if (points > 0)
        {
            retour = null;
        }
        else
        {
            GameObject.Find("LevelGenerator").GetComponent<LevelGenerator>().instantiateTerre(base.getGameObject().transform.position.x, base.getGameObject().transform.position.y, base.getGameObject().transform.rotation);
            Object.Destroy(base.getGameObject());
            retour = this;
        }

        return retour;

    }

}
