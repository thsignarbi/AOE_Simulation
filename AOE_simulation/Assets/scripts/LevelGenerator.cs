﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelGenerator : MonoBehaviour {

	[Header("Parameters")]
	public int tileAmount;
	private float tileSize = 1;
	private float waitingTime = 0;
	private float minX = 9999999;
	private float maxX = 0;
	private float minY = 9999999;
	private float maxY = 0;
	private float xAmount;
	private float yAmount;
	public int extraTreeX;
	public int extraTreeY;
	public int foodFrequency;


    [Header("Game objects")]
    public GameObject arbre1;
    public GameObject arbre2;
    public GameObject terre;
    public GameObject bleu;
    public GameObject rouge;
    public GameObject food;
    public GameObject bundary;
    public GameObject BC_up;
    public GameObject BC_down;
    public GameObject BC_left;
    public GameObject BC_right;

    public List<Vector3> createdTiles;

    private int[,] map;
    private List<Arbre> listArbres;
    private List<Buisson> listBuissons;


	void Awake () {
        createdTiles = new List<Vector3>();
        listArbres = new List<Arbre>();
        listBuissons = new List<Buisson>();
        if (tileAmount < 3)
        {
            tileAmount = 3;
        }
        StartCoroutine(GenerateMap());
	}

    IEnumerator GenerateMap()
    {

        for(int i = 0; i < tileAmount; ++i)
        {
            int dir = Random.Range(0, 4);

            createTile();
            MoveGen(dir);

           // yield return new WaitForSeconds(waitingTime);

        if(i == tileAmount - 1)
            {
                Finish();
            }
        }
        yield return 0;
    }

    void MoveGen(int dir)
    {
        switch (dir)
        {
            case 0: //Up
                transform.position = new Vector3(transform.position.x, transform.position.y + tileSize, 0);
                break;
            case 1: //Right
                transform.position = new Vector3(transform.position.x + tileSize, transform.position.y, 0);
                break;
            case 2: //Down
                transform.position = new Vector3(transform.position.x, transform.position.y - tileSize, 0);
                break;
            case 3: //Left
                transform.position = new Vector3(transform.position.x - tileSize, transform.position.y, 0);
                break;
        }
    }

    void createTile()
    {
        if (!createdTiles.Contains(transform.position))
        {
            GameObject[] tileObject = new GameObject[13];
            int counter = 0;

            for(int i = -1; i < 2; ++i)
            {
                for(int j = -1; j < 2; ++j)
                {
                    tileObject[counter] = Instantiate(terre, new Vector3(transform.position.x + i, transform.position.y + j, 0), transform.rotation) as GameObject;
                    createdTiles.Add(tileObject[counter].transform.position);
                    ++counter;
                }
            }
            
            tileObject[9] = Instantiate(terre, new Vector3(transform.position.x - 2, transform.position.y, 0), transform.rotation);
            tileObject[10] = Instantiate(terre, new Vector3(transform.position.x + 2, transform.position.y, 0), transform.rotation);
            tileObject[11] = Instantiate(terre, new Vector3(transform.position.x, transform.position.y + 2, 0), transform.rotation);
            tileObject[12] = Instantiate(terre, new Vector3(transform.position.x, transform.position.y - 2, 0), transform.rotation);

            createdTiles.Add(tileObject[8].transform.position);
            //Debug.Log(tileObject[8].transform.position.x + " , " + tileObject[8].transform.position.y + " , " + tileObject[8].transform.position.z);
            createdTiles.Add(tileObject[9].transform.position);
            createdTiles.Add(tileObject[10].transform.position);
            createdTiles.Add(tileObject[11].transform.position);
            createdTiles.Add(tileObject[12].transform.position);
        }
        else
        {
            tileAmount++;
        }
    }

    void Finish()
    {
        CreateWallsValues();
        CreateMap();
        CreateWalls();
        CreateBases();
        CreateFood();
        CreateBundaries();
    }

    void CreateWallsValues()
    {
        for(int i = 0; i < createdTiles.Count; ++i)
        {
            if(createdTiles[i].y < minY)
            {
                minY = createdTiles[i].y;
            }

            if(createdTiles[i].y > maxY)
            {
                maxY = createdTiles[i].y;
            }

            if(createdTiles[i].x < minX)
            {
                minX = createdTiles[i].x;
            }

            if(createdTiles[i].x > maxX)
            {
                maxX = createdTiles[i].x;
            }
        }

        xAmount = ((maxX - minX) / tileSize) + extraTreeX +1;
        yAmount = ((maxY - minY) / tileSize) + extraTreeY +1;
        Debug.Log("x size : " + xAmount);
        Debug.Log("y size : " + yAmount);
        map = new int[(int)xAmount,(int)yAmount];
    }

    void CreateMap()
    {
        for (int i = (int)(minX - (extraTreeX/2)); i < (int)(maxX + (extraTreeX/2))-1; ++i)
        {
            for(int j = (int)(maxY + (extraTreeY/2)); j > (int)(minY - (extraTreeY/2))-1; --j)
            {
                if (createdTiles.Contains(new Vector3((float)i, (float)j, 0)))
                {
                    map[worldToArrayX(i), worldToArrayY(j)] = 0; //Terre
                }
                else
                {
                    map[worldToArrayX(i), worldToArrayY(j)] = 1; //Arbre
                }
            }
        }
    }

    void CreateWalls()
    {
        Debug.Log("min X, max Y : " + (minX - (extraTreeX/2)) + ", " + (maxY+(extraTreeY/2)));

        int arbre;
        GameObject gameObject = null;

        for (int i = 0; i < xAmount; ++i)
        {
            for(int j = 0; j < yAmount; ++j)
            {
                if(!createdTiles.Contains(new Vector3((minX-(extraTreeX*tileSize)/2) + (i*tileSize), (minY-(extraTreeY*tileSize)/2)+ (j * tileSize))))
                {
                    arbre = Random.Range(0, 2);
                    switch (arbre)
                    {
                        case 0:
                            gameObject = Instantiate(arbre1, new Vector3((minX - (extraTreeX * tileSize) / 2) + (i * tileSize), (minY - (extraTreeY * tileSize) / 2) + (j * tileSize), (float)-0.001), transform.rotation) as GameObject;
                            break;
                        case 1:
                            gameObject = Instantiate(arbre2, new Vector3((minX - (extraTreeX * tileSize) / 2) + (i * tileSize), (minY - (extraTreeY * tileSize) / 2) + (j * tileSize), (float)-0.001), transform.rotation) as GameObject;
                            break;
                    }
                    listArbres.Add(new Arbre(new Vector2(worldToArrayX((int)((minX - (extraTreeX * tileSize) / 2) + (i * tileSize))), worldToArrayY((int)((minY - (extraTreeY * tileSize) / 2) + (j * tileSize)))), 30, gameObject));
                }
            }
        }
    }

    void CreateBases()
    {
        bool trouve = false;
        int i = 0, j = 0;
        if (xAmount > yAmount) //monde horizontal
        {
            //Base bleu
            while (!trouve)
            {
                if(BaseOk(i,j))
                {
                    trouve = true;
                    Instantiate(bleu, new Vector3(arrayToWorldX(i), arrayToWorldY(j), (float)-0.002), transform.rotation);
                    map[(int)(i % xAmount), (int)(j % yAmount)] = 3; //Blue base
                }
                ++j;
                if (j % yAmount == 0)
                {
                    ++i;
                }
            }
            trouve = false;
            i = (int)xAmount-1;
            j = 0;
            //Base rouge
            while (!trouve)
            {
                if (BaseOk(i, j))
                {
                    trouve = true;
                    Instantiate(rouge, new Vector3(arrayToWorldX(i), arrayToWorldY(j), (float)-0.002), transform.rotation);
                    map[(int)(i % xAmount), (int)(j % yAmount)] = 4; //Red base
                }
                ++j;
                if (j % yAmount == 0)
                {
                    --i;
                }
            }
        }
        else //monde vertical
        {
            //Base bleu
            while (!trouve)
            {
                if (BaseOk(i, j))
                {
                    trouve = true;
                    Instantiate(bleu, new Vector3(arrayToWorldX(i), arrayToWorldY(j), (float)-0.002), transform.rotation);
                    map[(int)(i % xAmount), (int)(j % yAmount)] = 3; //Blue base
                }
                ++i;
                if (i % xAmount == 0)
                {
                    ++j;
                }
            }
            trouve = false;
            i = 0;
            j = (int)yAmount-1;
            //Base rouge
            while (!trouve)
            {
                if (BaseOk(i, j))
                {
                    trouve = true;
                    Instantiate(rouge, new Vector3(arrayToWorldX(i), arrayToWorldY(j), (float)-0.002), transform.rotation);
                    map[(int)(i % xAmount), (int)(j % yAmount)] = 4; //Red base
                }
                ++i;
                if (i % xAmount == 0)
                {
                    --j;
                }
            }
        }
        


    }

    bool BaseOk(int i, int j)
    {
        bool ok = false;
        if(map[(int)(i % xAmount), (int)(j % yAmount)] == 0
                && map[(int)((i + 1) % xAmount), (int)(j % yAmount)] == 0
                && map[(int)((i + 2) % xAmount), (int)(j % yAmount)] == 0
                && map[(int)((i - 1) % xAmount), (int)(j % yAmount)] == 0
                && map[(int)((i - 2) % xAmount), (int)(j % yAmount)] == 0
                && map[(int)(i% xAmount), (int)((j+1) % yAmount)] == 0
                && map[(int)(i % xAmount), (int)((j + 2) % yAmount)] == 0
                && map[(int)(i % xAmount), (int)((j - 1) % yAmount)] == 0
                && map[(int)(i % xAmount), (int)((j - 2) % yAmount)] == 0
                && map[(int)((i+1) % xAmount), (int)((j + 1) % yAmount)] == 0
                && map[(int)((i-1) % xAmount), (int)((j + 1) % yAmount)] == 0
                && map[(int)((i+1) % xAmount), (int)((j -1) % yAmount)] == 0
                && map[(int)((i-1) % xAmount), (int)((j - 1) % yAmount)] == 0)
        {
            ok = true;
        }

        return ok;
    }

    int arrayToWorldX(int x)
    {
        return (int)(x % xAmount) - (extraTreeX / 2) + (int)minX;
    }

    int arrayToWorldY(int y)
    {
        return (int)(maxY + (extraTreeY / 2)) - (int)(y % yAmount);
    }

    int worldToArrayX(int x)
    {
        return x + (extraTreeX / 2) - (int)minX;
    }

    int worldToArrayY(int y)
    {
        return (extraTreeY / 2) + (int)maxY - y;
    }
	
    void CreateFood()
    {

        GameObject gameObject = null;

        for(int i = 0+(extraTreeX/2); i < xAmount - 1 - (extraTreeX/2); ++i)
        {
            for(int j = 0+(extraTreeY/2); j < yAmount - 1-(extraTreeY/2); ++j)
            {
                if (map[i, j] == 0)
                {                    
                    float rand = Random.Range(0, foodFrequency);
                    //Debug.Log(rand);
                    if (rand <= 1)
                    {
                        gameObject = Instantiate(food, new Vector3(arrayToWorldX(i), arrayToWorldY(j), (float)-0.001), transform.rotation) as GameObject;
                        map[i,j] = 2; //Food

                        listBuissons.Add(new Buisson(new Vector2(i, j), 30, gameObject));
                    }
                }
            }
        }
    }

    void CreateBundaries()
    {
        float milX = ((minX - (extraTreeX / 2)) + (maxX + (extraTreeX / 2))) / 2;
        float milY = ((minY - (extraTreeY / 2)) + (maxY + (extraTreeY / 2))) / 2;
        //ALL THE MAP
        GameObject bund = Instantiate(bundary, new Vector3(milX, milY, 0), transform.rotation) as GameObject;
        bund.transform.localScale += new Vector3(xAmount-0.5f, yAmount-0.5f, 0);
        //COLLIDER UP
        GameObject BCUP = Instantiate(BC_up, new Vector3(milX, milY + (yAmount/2)+2, 0), transform.rotation) as GameObject;
        BCUP.transform.localScale = new Vector3(xAmount, 0.5f, 1);
        //COLLIDER DOWN
        GameObject BCDOWN = Instantiate(BC_down, new Vector3(milX, milY - (yAmount / 2), 0), transform.rotation) as GameObject;
        BCDOWN.transform.localScale = new Vector3(xAmount, 0.5f, 1);
        //COLLIDER LEFT
        GameObject BCLEFT = Instantiate(BC_left, new Vector3(milX - (xAmount / 2)+1.25f, milY, 0), transform.rotation) as GameObject;
        BCLEFT.transform.localScale = new Vector3(0.5f, yAmount, 1);
        //COLLIDER RIGHT
        GameObject BCRIGHT = Instantiate(BC_right, new Vector3(milX + (xAmount / 2)-1.25f, milY, 0), transform.rotation) as GameObject;
        BCRIGHT.transform.localScale = new Vector3(0.5f, yAmount, 1);

        //SET CAMERA IN THE MIDDLE
        Camera.main.transform.position = new Vector3(milX, milY, Camera.main.transform.position.z);
    }

    //GETTERS
    public int[,] getMap()
    {
        return map;
    }

    public float getXAmount()
    {
        return xAmount;
    }

    public float getYAmount()
    {
        return yAmount;
    }

    public int getExtraTreeX()
    {
        return extraTreeX;
    }

    public int getExtraTreeY()
    {
        return extraTreeY;
    }

    public float getMinX()
    {
        return minX;
    }

    public float getMaxY()
    {
        return maxY;
    }

    public List<Arbre> getListArbres()
    {
        return listArbres;
    }

    public List<Buisson> getListBuissons()
    {
        return listBuissons;
    }

    public int getTileAmount()
    {
        return tileAmount;
    }

    public void instantiateTerre(float x, float y, Quaternion rotation)
    {
        Instantiate(terre, new Vector3(x, y, 0), rotation);
    }
}
