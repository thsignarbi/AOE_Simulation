﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Humain : Agent{
    private int tribu;
    private int sexe;
    private float age = 0;
    private Activite activite;
    private GameObject gameObject;
    private float force;

    public static float TEMPS = 0.10f;

	public Humain(int tribu, int sexe, float age, GameObject gameObject, Vector2 position, float health, float force):base(position, health)
    {
        this.tribu = tribu;
        this.sexe = sexe;
        this.activite = null;
        this.age = age;
        this.gameObject = gameObject;
        switch (sexe)
        {
            case 1: //homme
				this.force = force;
                break;
            case 2: //femme
				this.force = 0.75f*force;
                break;
        }      
    }


    //GETTERS
    public int getTribu()
    {
        return tribu;
    }

    public int getSexe()
    {
        return sexe;
    }

    public float getAge()
    {
        return age;
    }

    public Activite getActivite()
    {
        return activite;
    }

    public GameObject getGameObject()
    {
        return gameObject;
    }

    public float getForce()
    {
        return force;
    }

    //SETTERS

    public void setActivite(Activite act)
    {
        activite = act;
    }

    //METHODS
    public Humain isNextToEnemy(List<Humain> humains)
    {
        Humain retour = null;
       foreach(Humain other in humains)
        {
            if(other != null && other.getTribu() != getTribu() && Vector3.Distance(getGameObject().transform.position, other.getGameObject().transform.position) < 2.5)
            {
                retour = other;
            }
        }

        return retour;
    }

    public Humain recoitCoup(Humain other)
    {
        Humain retour;

        if(activite != null && activite.GetType() != typeof(SeBattre))
        {
            activite = new SeBattre(this, other);
        }

        if (getHealth() > 0)
        {
            base.decreaseHealth(other.getForce());
        }

        if (getHealth() > 0)
        {
            retour = null;
        }
        else
        {
            Object.Destroy(getGameObject());
            retour = this;
        }

        return retour;
    }

    static int arrayToWorldX(int x, int xAmount, int extraTreeX, int minX)
    {
        return (int)(x % xAmount) - (extraTreeX / 2) + (int)minX;
    }

    static int arrayToWorldY(int y, int yAmount, int extraTreeY, int maxY)
    {
        return (int)(maxY + (extraTreeY / 2)) - (int)(y % yAmount);
    }

    public void setActivityOver()
    {
        activite = null;
    }

    public void vieillit()
    {
        age += TEMPS;
    }

}



