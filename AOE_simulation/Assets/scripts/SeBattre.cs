﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SeBattre : Activite
{

    Humain other;
    public static float SPEED = 5f;

    public SeBattre(Humain humain, Humain other) : base("Se battre", 5, humain)
    {
        this.other = other;
    }

    override
    public Objet execute()
    {
        Objet retour;
        if (other == null)
        {
            retour = new Humain(0, 0, 0, null, new Vector2(), 0, 0);
        }
        else if (other.getGameObject() == null)
        {
            retour = other;
        }
        else
        {
            if (Vector3.Distance(base.getHumain().getGameObject().transform.position, other.getGameObject().transform.position) > 1)
            {
                base.getHumain().getGameObject().transform.position = Vector3.MoveTowards(base.getHumain().getGameObject().transform.position, other.getGameObject().transform.position, SPEED * Time.deltaTime);
                retour = null;
            }
            else
            {
                if(other == null || getHumain() == null)
                {
                    retour = new Humain(0, 0, 0, null, new Vector2(), 0, 0);
                }
                else
                {
                    retour = other.recoitCoup(getHumain());
                }
                
            }
        }


        return retour;
    }

}
