﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Buisson : AgentPassif
{
    public Buisson(Vector2 position, int points, GameObject go) : base(position, points, go)
    {

    }

    public AgentPassif cueille()
    {
        AgentPassif retour;

        if(points > 0)
        {
            points--;
        }

        if(points > 0)
        {
            retour = null;
        }
        else
        {
            GameObject.Find("LevelGenerator").GetComponent<LevelGenerator>().instantiateTerre(base.getGameObject().transform.position.x, base.getGameObject().transform.position.y, base.getGameObject().transform.rotation);
            Object.Destroy(base.getGameObject());
            retour = this;
        }

        return retour;
    }



}
