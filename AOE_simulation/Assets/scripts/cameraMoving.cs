﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cameraMoving : MonoBehaviour {

    
    public float speed = 5.0f;
    private float dist;
    public float minX;
    public float maxX;
    public float minY;
    public float maxY;

    private bool moveUp;
    private bool moveDown;
    private bool moveLeft;
    private bool moveRight;

    private void Start()
    {
        moveUp = true;
        moveDown = true;
        moveLeft = true;
        moveRight = true;
    }


    void Update() {



        Vector3 newCameraPosition = transform.position;
        if (Input.GetKey(KeyCode.RightArrow) && moveRight)
        {
            newCameraPosition = new Vector3(transform.position.x + speed * Time.deltaTime, transform.position.y + 0, transform.position.z + 0);
            moveLeft = true;
        }
        if (Input.GetKey(KeyCode.LeftArrow) && moveLeft)
        {
            newCameraPosition = new Vector3(transform.position.x - speed * Time.deltaTime, transform.position.y + 0, transform.position.z + 0);
            moveRight = true;
        }
        if (Input.GetKey(KeyCode.DownArrow) && moveDown)
        {
            newCameraPosition = new Vector3(transform.position.x + 0, transform.position.y - speed * Time.deltaTime, transform.position.z + 0);
            moveUp = true;
        }
        if (Input.GetKey(KeyCode.UpArrow) && moveUp)
        {
            newCameraPosition = new Vector3(transform.position.x + 0, transform.position.y + speed * Time.deltaTime, transform.position.z + 0);
            moveDown = true;
        }

        transform.position = newCameraPosition;
        

 }

    void OnTriggerEnter2D(Collider2D other)
    {

        if (other.tag == "GameBundary_up")
        {
            moveUp = false;
        }
        if(other.tag == "GameBundary_down")
        {
            moveDown = false;
        }
        if (other.tag == "GameBundary_left")
        {
            moveLeft = false;
        }
        if (other.tag == "GameBundary_right")
        {
            moveRight = false;
        }

    }

    void OnTriggerStay2D(Collider2D coll)
    {
        if (coll.tag == "GameBundary_up")
        {
            moveUp = false;
        }
        if (coll.tag == "GameBundary_down")
        {
            moveDown = false;
        }
        if (coll.tag == "GameBundary_left")
        {
            moveLeft = false;
        }
        if (coll.tag == "GameBundary_right")
        {
            moveRight = false;
        }

    }

}
